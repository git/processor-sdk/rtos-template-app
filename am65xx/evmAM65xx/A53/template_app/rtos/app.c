/*
 * Copyright (C) 2017-2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the
 * distribution.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of
 * its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/**
 *
 *  \brief  Template application tasks file:
 *          This template application exercises multiple tasks and
 *          peripherals. The different task functions are run under
 *          separate Tasks in TI BIOS.
 *          The appTasksCreate function creates the different tasks.
 *          More tasks can be added in this function as required.
 */

/* Standard header files */
#include <string.h>

/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Error.h>

/* Local template app header file */
#include "app.h"

/**********************************************************************
 ************************** Function prototypes ***********************
 **********************************************************************/
void biosTaskCreate(ti_sysbios_knl_Task_FuncPtr taskFunctionPtr,
                    char *taskName, int taskPriority, int stackSize);

/* Task functions */
void gpio_toggle_led_task(UArg arg0, UArg arg1);
void uart_task(UArg arg0, UArg arg1);
void spi_test_task(UArg arg0, UArg arg1);
void i2c_eeprom_read_and_display_task(UArg arg0, UArg arg1);
/* Add any additional task function prototypes here */

/**********************************************************************
 ************************** Global Variables **************************
 **********************************************************************/
volatile uint32_t g_endTestTriggered = 0;

/**
 *  @brief Function appTasksCreate : Creates multiple application tasks
 *
 *  @param[in]         None
 *  @retval            none
 */
void appTasksCreate(void)
{

    /* Create multiple tasks with different task priority & stack size */
    appPrint("\n ======== Starting to create application tasks ========\n");

    /* Create task to toggle led */
    biosTaskCreate(gpio_toggle_led_task,
                   "gpio_toggle_led_task",
                   9, 4096);

    /* Create task to exercise uart */
    biosTaskCreate(uart_task,
                   "uart_task",
                   8, 4096);

    /* Create task to test spi interface */
    biosTaskCreate(spi_test_task,
                   "spi_test_task",
                   6, 4096);

    /* Create task to test spi interface */
    biosTaskCreate(i2c_eeprom_read_and_display_task,
                   "i2c_eeprom_read_and_display_task",
                   7, 4096);

    /* Add additional tasks here */

    appPrint("\n ======== Application tasks created successfully ========\n");

}

/**
 *  @brief Function gpio_toggle_led_task : Toggles LED through GPIO
 *
 *  @param[in]         arg0, arg1: Arguments ( Currently not used)
 *  @retval            none
 */
void gpio_toggle_led_task(UArg arg0, UArg arg1)
{

    appPrint("\n gpio_toggle_led task started");
    while(1) {
        /* Toggle test GPIO connected to LED */
        GPIO_toggle(TEST_LED_GPIO_INDEX);

        /* Delay to set period of pulse */
        Task_sleep(LED_BLINK_DELAY_VALUE);

        /* If end Test is triggered, then exit
         * Note:  The end test can be triggered through commands through uart
         */
        if (g_endTestTriggered)
            break;
    };
    appPrint("\n gpio_toggle_led task ended");
    Task_exit();
}

/**
 *  @brief Function uart_task : This task scans UART port and prints
 *      back word entered. On "ESC" triggers end of test.
 *      Exercises reads and writes to UART port
 *
 *  @param[in]         arg0, arg1: Arguments ( Currently not used)
 *  @retval            none
 */
void uart_task(UArg arg0, UArg arg1)
{

    char buffPointer[1000];
    char echoPrompt[] = "\n uart_task :Enter a word or Esc to quit >";
    char echoPrompt1[] = "Data received is:";

    appPrint("\n uart_task task started\n");
    
    /* Wait for other tasks to settle */
    Task_sleep(100);

    memset(buffPointer, 0, sizeof(buffPointer));
    while (1) {
        /* Print prompt to UART port */
        UART_printf(echoPrompt);

        /* Scan input word from user */
        if (UART_scanFmt("%s", &buffPointer) != S_PASS)
        {
            appPrint("\n ERROR: UART_scanFmt failed");
            goto UART_TASK_EXIT;
        }

        /* Check on word entered here */
        /* If needed a command parser can be added here */
        switch(buffPointer[0]) {
        case '\0': /*Ignore empty string */
            break;

        case 27: /* Exit on ESC character */
            goto UART_TASK_EXIT;

        default:
            /* Display prompt  */
            UART_printf(echoPrompt1);
            /* Display received word */
            UART_printf(buffPointer);
            /* Print new line */
            UART_printf("\n");
            break;
        }

        /* Sleep to yield */
        Task_sleep(10);
    }

UART_TASK_EXIT:
    appPrint("\n uart_task task ended");
    /* Trigger end test to other tasks */
    g_endTestTriggered = 1;
    /* Wait a while */
    Task_sleep(100);
    /* Now exit application */
    BIOS_exit(0);
    Task_exit();
    return;
}

/**
 *  @brief Function spi_test_task : Execute read on SPI bus
 *
 *  @param[in]         arg0, arg1: Arguments ( Currently not used)
 *  @retval            none
 */
void spi_test_task(UArg arg0, UArg arg1)
{
    SPI_Params   spiParams;              /* SPI params structure */

    appPrint("\n spi_test task started");

    /* Default SPI configuration parameters */
    SPI_Params_init(&spiParams);

    /* TODO: Add SPI functionality test here */
    Task_sleep(10);
    appPrint("\n spi_test task ended");
    Task_exit();
}

/**
 *  @brief Function i2c_eeprom_read_and_display_task :
 *      Reads eeprom contents through I2C and prints Board version
 *
 *  @param[in]         arg0, arg1: Arguments ( Currently not used)
 *  @retval            none
 */
void i2c_eeprom_read_and_display_task(UArg arg0, UArg arg1)
{

    Board_IDInfo_v2 info = {0};
    Board_STATUS status;

    status = Board_getIDInfo_v2(&info, BOARD_I2C_EEPROM_ADDR);

    if(status != BOARD_SOK)
    {

        if(status==BOARD_INVALID_PARAM)
        {
          appPrint("\n Board info is not supported!");
          goto i2c_eeprom_read_and_display_task_exit;
        }
        else
        {
          appPrint("\n Board info read failed!!");
          goto i2c_eeprom_read_and_display_task_exit;
        }
    }


    appPrint("\n Board Name read: %s\n",info.boardInfo.boardName);
     /* Get board version */
    appPrint("\n Board version read: %s\n",info.boardInfo.designRev);

    Task_sleep(10);
i2c_eeprom_read_and_display_task_exit:
    Task_exit();
}

/**
 *  @brief Function biosTaskCreate : Task create function
 *             This function is customized to control
 *             certain parameters of the tasks. More parameters
 *             can be controlled for each task; Refer to SYSBIOS
 *             API for other parameters that can be controlled.
 *  @retval              : 0: success ; -1: fail
 */
void biosTaskCreate(ti_sysbios_knl_Task_FuncPtr taskFunctionPtr,
                    char *taskName, int taskPriority, int stackSize)
{
    Task_Params taskParams;
    Error_Block eb;
    Task_Handle task;

    Error_init(&eb);
    Task_Params_init(&taskParams);
    taskParams.instance->name = taskName;
    taskParams.priority = taskPriority;
    taskParams.stackSize = stackSize;
    task = Task_create(taskFunctionPtr, &taskParams, &eb);
    if (task == NULL) {
       appPrint("%s: Task_create() failed! \n", taskName);
       BIOS_exit(0);
    }
    appPrint("\n %s task created.", taskName);

    return;
}
