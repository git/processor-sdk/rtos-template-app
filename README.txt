Overview
--------
This template application provides a basic project to start your development
and it showcases exercising multiple tasks and multiple peripherals in the SOC.
The template app is expected to be used by directly importing the projects into
CCS development environment.
NOTE1: The app is created for each platform separately. Look at comments in the
source files for more details starting from main.c for the sequence from beginning.

NOTE2: Some platform specific directories may not be present in a platform specific
SDK.

To customize the application, you can start with modifying just the application
specific files: app.*

The directory structure is as follows:
├── LICENSE.txt --> License text file
├── README.txt  --> This Readme file.
├── common --> Files to set common preferred versions
│   ├── cgt_a15_ver.dtd --> sets code generation tool version for a15
│   ├── cgt_a8_ver.dtd --> sets code generation tool version for a8
│   └── xdc_ver.dtd  --> sets xdc version
├── am572x
│   ├── components_am57xx.dtd --> Sets preferred component version for am57xx
│   └── evmAM572X
│       └── A15
│           └── template_app
│               ├── main.c --> Main file which has startup and init code
│               ├── main.cfg --> Main BIOS config file
│               ├── app.c  --> Application tasks are here
│               ├── app.h  --> Application header file
│               ├── app.cfg --> Application specific config file
│               ├── app.defs --> Definitions (Currently empty)
│               ├── rtos_template_app_am572x_a15_evmAM572X.projectspec --> Project spec file
│               ├── GPIO_board.h --> GPIO board header
│               ├── GPIO_evmAM572x_board.c --> Gpio board source
├── am335x
│   ├── components_am335x.dtd --> Sets preferred component version for am335x
│   └── evmAM335X
│       └── A8
│           └── template_app
│               ├── main.c --> Main file which has startup and init code
│               ├── main.cfg --> Main BIOS config file
│               ├── app.c  --> Application tasks are here
│               ├── app.h  --> Application header file
│               ├── app.cfg --> Application specific config file
│               ├── app.defs --> Definitions (Currently empty)
│               ├── rtos_template_app_am335x_a8_evmAM335X.projectspec --> Project spec file


How to use the rtos_template_app
--------------------------------
Pre-requisites: The use of this requires CCS v7 and Processor SDK RTOS installed.

The rtos template app can be imported directly into CCS to build and run.
Importing of the project will create a new project under your workspace
and copy the relevant source files to the project.

See instruction on how to connect the JTAG debugger for a particular platform
in the Processor SDK Documentation
