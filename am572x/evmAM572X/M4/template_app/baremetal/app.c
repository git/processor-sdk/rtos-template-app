/*
 * Copyright (C) 2019 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the
 * distribution.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of
 * its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/**
 *
 *  \brief  Template application tasks file:
 *          This template application exercises multiple peripherals.
 *          The different task functions are run as part of the
 *          appRunTasksFunction().
 *          More tasks can be added in this function as required.
 */

/* Standard header files */
#include <string.h>

/* Local template app header file */
#include "app.h"

/**********************************************************************
 ************************** Macros ************************************
 **********************************************************************/
#define GPTIMER3                    2
#define TIMER_ID                    GPTIMER3


/**********************************************************************
 ************************** Global Variables **************************
 **********************************************************************/
volatile uint32_t g_endTestTriggered = 0;
volatile unsigned int delayTimerFlag;
TimerP_Handle   delayTimerHandle;

/**********************************************************************
 ************************** Function prototypes ***********************
 **********************************************************************/
/*
 * Task functions:
 * Note that these functions are called "tasks" for naming consistency
 * with the RTOS template application, but they are not actual SYS/BIOS
 * tasks.
 */
void uart_task();
void spi_test_task();
void i2c_eeprom_read_and_display_task();
void gpio_toggle_led_task();

/**
 *  @brief Function appRunTasks : Runs various application tasks.
 *
 *  @retval            none
 */
void appRunTasks()
{
    i2c_eeprom_read_and_display_task();
    spi_test_task();
    while(!g_endTestTriggered)
    {
        gpio_toggle_led_task();
        uart_task();
    }
}

/**
 *  @brief Function delayTmrIsr : ISR triggered by timer for GPIO task
 *
 *  @retval            none
 */
void delayTmrIsr(void* arg)
{
    delayTimerFlag = 1;
}

/**
 *  @brief Function osalTimerStart : Starts the timer for GPIO task
 *
 *  @retval            none
 */
void osalTimerStart(unsigned int ms)
{
    delayTimerFlag  = 0;

    TimerP_setPeriodMicroSecs(delayTimerHandle,
                              (uint32_t)(ms*1000));

    TimerP_start(delayTimerHandle);
}

/**
 *  @brief Function osalTimerDelay : blocking delay function.
 *      The application will get stuck here if the timer is
 *      failing to trigger its ISR.
 *
 *  @retval            none
 */
void osalTimerDelay(unsigned int ms)
{
    osalTimerStart(ms);
    while (delayTimerFlag == 0);
}

/**
 *  @brief Function gpio_toggle_led_task : Toggles LED through GPIO
 *      once per second.
 *
 *  @retval            none
 */
void gpio_toggle_led_task()
{
    static int gpio_is_init = 0;
    static int pinval = GPIO_PIN_VAL_LOW;

    /* If this is the first time calling this task, then run through the
     * initialization procedure.
     */
    if (!gpio_is_init)
    {
        TimerP_Params   delayTimerParams;

        /* Create a continuous timer */
        TimerP_Params_init(&delayTimerParams);
        delayTimerParams.period = 1000000;  // 1s
        delayTimerParams.periodType = TimerP_PeriodType_MICROSECS;
        delayTimerParams.arg = 0;
        delayTimerParams.startMode = TimerP_StartMode_USER;
        delayTimerParams.runMode = TimerP_RunMode_CONTINUOUS;
        delayTimerHandle = TimerP_create(TIMER_ID,
                                         (TimerP_Fxn)&delayTmrIsr,
                                         &delayTimerParams);
        if (delayTimerHandle == NULL) {
            appPrint("Timer create failed\n");
        }

        /* Start the timer */
        osalTimerDelay(1000);

        gpio_is_init = 1;
    }

    /* Toggle the LED whenever flag is raised */
    if (delayTimerFlag != 0)
    {
        if (pinval == GPIO_PIN_VAL_LOW)
        {
            GPIO_write(USER_LED1, GPIO_PIN_VAL_HIGH);
            pinval = GPIO_PIN_VAL_HIGH;
        }
        else
        {
            GPIO_write(USER_LED1, GPIO_PIN_VAL_LOW);
            pinval = GPIO_PIN_VAL_LOW;
        }
        delayTimerFlag  = 0;
    }
}

/**
 *  @brief Function uart_task : This task scans UART port and prints
 *      back the word entered. On "ESC" it triggers the end of test.
 *      Exercises reads and writes to UART port.
 *
 *  @retval            none
 */
void uart_task()
{
    static int uart_is_init = 0;
    static char buffPointer[1000];
    static const char echoPrompt[] = "\n uart_task :Enter a word or Esc to quit >";
    static const char echoPrompt1[] = "Data received is:";
    static char character;
    static int index;

    /* If this is the first time calling this task, then run through the
     * initialization procedure.
     */
    if (!uart_is_init)
    {
        /* Get default UART params */
        UART_Params uartParams;
        UART_Params_init(&uartParams);

        /* Set a read timeout so that UART read does not block other tasks */
        uartParams.readTimeout  = 50000U;

        /* De-init default UART instance and re-open with new params */
        UART_stdioDeInit();
        UART_stdioInit2(UART_INSTANCE, &uartParams);

        uart_is_init = 1;

        memset(buffPointer, 0, sizeof(buffPointer));

        appPrint("\n uart_task task started\n");

        /* Print prompt to UART port */
        UART_printf(echoPrompt);
    }

    /* Get one letter at a time so other tasks are not blocked waiting
     * for an entire string to be entered.
     */
    character = UART_getc();
    if (!character)
    {
        //Timeout. Do nothing.
        return;
    }
    else
    {
        /* Store each character in the buffer. When a newline is entered, print
         * the contents of the buffer and then clear it.
         */
        buffPointer[index] = character;
        /* Check on word entered here */
        /* If needed a command parser can be added here */
        switch(character) {
            case '\0': /*Ignore empty string */
                break;
            case 27: /* Exit on ESC character */
                goto UART_TASK_EXIT;
            case '\n': /* Print back entire line */
                UART_putc(character);
                /* Display prompt  */
                UART_printf(echoPrompt1);
                /* Display received word */
                UART_printf(buffPointer);
                /* Print new line */
                UART_printf("\n");
                index = 0;
                memset(buffPointer, 0, sizeof(buffPointer));
                UART_printf(echoPrompt);
                break;
            default:
                /* Echo back current character */
                UART_putc(character);
                index++;
                break;
        }
        return;
    }

UART_TASK_EXIT:
    appPrint("\n uart_task task ended");
    /* Trigger end test to other tasks */
    g_endTestTriggered = 1;
    return;
}

/**
 *  @brief Function spi_test_task : Execute read on SPI bus
 *
 *  @retval            none
 */
void spi_test_task()
{
    SPI_Params   spiParams;              /* SPI params structure */

    appPrint("\n spi_test task started");
    /* Default SPI configuration parameters */
    SPI_Params_init(&spiParams);

    /* TODO: Add SPI functionality test here */
    appPrint("\n spi_test task ended");
}

/**
 *  @brief Function i2c_eeprom_read_and_display_task :
 *      Reads eeprom contents through I2C and prints Board version
 *
 *  @retval            none
 */
void i2c_eeprom_read_and_display_task()
{
    I2C_Params i2cParams;
    I2C_Handle handle = NULL;
    I2C_Transaction i2cTransaction;
    bool status;
    char txBuf[2] = {0x00, 0x00};
    char boardName[20];
    char boardVersion[20];

    appPrint("\n i2c_eeprom_read_and_display task started");

    /* Initialize parameters */
    I2C_Params_init(&i2cParams);

    /* Open I2C instance */
    handle = I2C_open(BOARD_I2C_EEPROM_INSTANCE, &i2cParams);

    /* Configure common parameters with I2C transaction */
    i2cTransaction.slaveAddress = BOARD_I2C_EEPROM_ADDR;
    i2cTransaction.writeBuf = (uint8_t *)&txBuf[0];
    i2cTransaction.writeCount = 2;

    /* Get board name */
    txBuf[0] = (char)(((uint32_t) 0xFF00 & BOARD_EEPROM_BOARD_NAME_ADDR)>>8);
    txBuf[1] = (char)((uint32_t) 0xFF & BOARD_EEPROM_BOARD_NAME_ADDR);
    i2cTransaction.readBuf = boardName;
    i2cTransaction.readCount = BOARD_EEPROM_BOARD_NAME_LENGTH;
    status = I2C_transfer(handle, &i2cTransaction);
    if (status == false)
    {
        I2C_close(handle);
        appPrint("\n ERROR: I2C_transfer failed");
        goto I2C_TEST_EXIT;
    }
    boardName[BOARD_EEPROM_BOARD_NAME_LENGTH] = '\0';
    appPrint("\n Board Name read: %s", boardName);

    /* Get board version */
    txBuf[0] = (char)(((uint32_t) 0xFF00 & BOARD_EEPROM_VERSION_ADDR)>>8);
    txBuf[1] = (char)((uint32_t) 0xFF & BOARD_EEPROM_VERSION_ADDR);
    i2cTransaction.readBuf = boardVersion;
    i2cTransaction.readCount = BOARD_EEPROM_VERSION_LENGTH;
    status = I2C_transfer(handle, &i2cTransaction);
    if (status == false)
    {
        I2C_close(handle);
        appPrint("\n ERROR: I2C_transfer failed");
        goto I2C_TEST_EXIT;
    }
    boardVersion[BOARD_EEPROM_VERSION_LENGTH] = '\0';
    appPrint("\n Board version read: %s", boardVersion);
    I2C_close(handle);

I2C_TEST_EXIT:
    appPrint("\n i2c_eeprom_read_and_display task ended");
}
