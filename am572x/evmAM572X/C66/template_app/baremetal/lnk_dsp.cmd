/*
 * Copyright (C) 2019 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the
 * distribution.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of
 * its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

-stack  0x1000                             /* SOFTWARE STACK SIZE           */
-heap   0x2000                             /* HEAP AREA SIZE                */

/* SPECIFY THE SYSTEM MEMORY MAP */

MEMORY
{
        RST_START:      org = 0x00800000  len = 0x0300
        IRAM_MEM:       org = 0x00800300  len = 0x7c00
        MMU_TLB:		ORIGIN = 0x4031C000  LENGTH = 0x000004000
		/*SBL will use 1 KB of space from address 0x80000000 for EVE */
        DDR3_A8:		org = 0x80000400 len = (0x0B000000 - 0x400)    /* 176 MB */
        DDR3_BOOT:      org = 0x8B000000 len = 0x00010000    /* 32 MB */
		DDR3_DSP:		org = 0x8B010000 len = 0x01FF0000    /* 32 MB */
		DDR3_M3VPSS:	org = 0x8D000000 len = 0x01000000    /* 16 MB */
		DDR3_M3VIDEO:	org = 0x8E000000 len = 0x01000000    /* 16 MB */
		DDR3_SR0:		org = 0x8F000000 len = 0x01000000    /* 16 MB */
}

/* SPECIFY THE SECTIONS ALLOCATION INTO MEMORY */

SECTIONS
{
	boot :
	{
	 rts*.lib<boot*.obj>(.text)
	}load > DDR3_BOOT

	.cachetest_ddrbuf > DDR3_DSP
    GROUP: load > DDR3_DSP
    {
        .bss:       /* UNINITIALIZED OR ZERO INITIALIZED */
        .neardata:
        .rodata:
    }
    BOARD_IO_DELAY_DATA : load > IRAM_MEM
    BOARD_IO_DELAY_CODE : load > IRAM_MEM
    .csl_vect : load > RST_START
    .vects : load > IRAM_MEM
    .l2_int  : load > IRAM_MEM
    .pmIdleFunc : load > IRAM_MEM
    .init    : load > DDR3_DSP
    .cio     : load > DDR3_DSP

    .text 	 : load > DDR3_DSP              /* CODE                         */
    .data    : load > DDR3_DSP              /* INITIALIZED GLOBAL AND STATIC VARIABLES. */
                                            /* GLOBAL & STATIC VARIABLES.   */
                    RUN_START(bss_start)
                    RUN_END(bss_end)
    .const   : load > DDR3_DSP              /* GLOBAL CONSTANTS             */
    .cinit   : load > DDR3_DSP
    .stack   : load > DDR3_DSP            /* SOFTWARE SYSTEM STACK        */
	.far	 : load > DDR3_DSP
	.plt     : load > DDR3_DSP
	.fardata : load > DDR3_DSP
	.switch	 : load > DDR3_DSP
	.my_sect_ddr : load > DDR3_DSP
	.sysmem : load > DDR3_DSP
}

